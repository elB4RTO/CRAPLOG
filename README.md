# Craplog
A tool to create statistics from Apache2 log files

<br/>

![logo](https://git.disroot.org/elB4RTO/screenshots/raw/branch/main/Craplog/craplogo.png)

<br/>

## Table of contents

- [Overview](#overview)
- [Official versions](#official-versions)
- [Unmantained versions](#unmantained-versions)
- [Something more about Craplog](#more-about-craplog)

<br/>

## Overview

Craplog is a tool that takes Apache2 logs in their default form, parses them and creates simple statistics.

Please refer to one of the available versions for more informations.

<br/>

## Official versions

| Version | Repository | Description | Coded with |
| :-----: | :--------: | :---------- | :--------: |
|    -    |      -     |      -      |      -     |

<br/>At the moment all the flavors of Craplog have been archived or are unmantained, but there's a good news!<br/>
**Craplog has reborn into [LogDoctor](https://git.disroot.org/elB4RTO/LogDoctor)**, a more powerful and stylish fork!

<br/><br/>

### Unmantained versions

Old versions, which still work just fine, but they're no more getting updated.

| Version | Repository | Description | Coded with |
| :-----: | :--------: | :---------- | :--------: |
| 3.0 | [CLI](https://git.disroot.org/elB4RTO/craplog-CLI) | Command line version    | ![Python](https://img.shields.io/badge/%20-Python-3572A5) |

*Help wanted to keep them alive.*

<br/><br/>

### Archived versions

| Version | Repository | Description | Coded with |
| :-----: | :--------: | :---------- | :--------: |
| 5.0 | [GUI](https://git.disroot.org/elB4RTO/craplog-GUI) | Fully graphical version | ![Java](https://img.shields.io/badge/%20-Java-b07219) |

<br/><br/>

## More about Craplog

Craplog has born in early 2021 as a private project: it was a simple bash script with the aim of cleaning Apache2 logs, but it has evolved up to the current stage.

It is now a more complex, public domain project: it makes statistics, allows to view them, has more advanced features to customize it and the way it behaves, it's available in both a GUI version (written in Java) and a CLI version (written in Python), and it keeps growing.

<br/>
